package com.twuc.webApp.contracts;

public class ErrorResponse {
    private final String timestamp;
    private final String error;
    private final String errorType;

    public ErrorResponse(String timestamp, String error, String errorType) {
        this.timestamp = timestamp;
        this.error = error;
        this.errorType = errorType;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getError() {
        return error;
    }

    public String getErrorType() {
        return errorType;
    }
}
